FROM openresty/openresty:stretch
LABEL org.label-schema.schema-version="1.0.0"
LABEL org.label-schema.vendor="Sitepilot"
LABEL org.label-schema.name="app0000"

ADD htdocs /usr/local/openresty/nginx/html
ADD default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80